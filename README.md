# GUILDS Online Core Rulebook

This is the Work in Progress Online Core Rulebook (WIPOCR).

Live Web Version: https://johncmcgrew.gitlab.io/guilds-online-rulebook/

July 9 v alpha 0.0.1
To do:
1. Author draft text of rules document using **Google Docs**
    https://docs.google.com/document/d/1p94p0jG2pfJLrNrl-JAFUjzm-0aSC0fQtyEiJuxqIG4/edit?usp=sharing
2. Once verbiage is more or less complete, export using add-on
3. paste each section into Gitlab repo and organize
4. edit css files
