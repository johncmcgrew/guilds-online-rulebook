
# Artisan Guild Alpha Rulebook

<img 
    style="display: block; 
           margin-left: auto;
           margin-right: auto;"
    src="./_static/banner.png"
    alt="Banner">
</img>

This is the Alpha build of GUILDS, the Artisan Guild Skirmish Wargame. There's a lot of changes in the works, and some game terms and unit cards are being redesigned.

# Introduction

Welcome to the universe of **Artisan Guild**!

***GUILDS*** is a tabletop miniatures skirmish wargame set in an intriguing arcane world called **Mundus**, a place inhabited by mythic creatures and scourged by warfare.

You take the role of a **Guildmaster**, a cunning or resolute leader of a rising **Guild**.
As you build your **Guildhall**, you’ll recruit **Infantry** warriors, mounted **Cavalry** units, and stalwart **Heroes** and **Heroic Beasts**.
It’s up to you to lead your warband to face grueling battles against rival guilds, epic monstrosities and other sinister forces.

Throughout a **Campaign**, you’ll embark on **Quests** to compete with other guilds for spoils and treasure.
Each **Skirmish** risks defeat and setback, but also the opportunity to seize unique relics and gain new powers.
As your renown grows and the influence of your guild spreads, you’ll have the chance to attract **Arcanists**, **Epic Heroes**, and even **Legendary Beasts** to your cause.

Five powerful **Forces** seek to conquer Mundus and bring the realm under their sway.
Will your guild stand against the tide of **Chaos** and **Corruption** in the name of **Order** and **Fortitude**, or will the **Wild** grow resurgent?
You and your guild may very well be the ones to tip the scales in the conflict for the fate of Mundus!

<div class="break-page"></div>

```{toctree}
  :maxdepth: 4
  :caption: Core Rulebook
  :hidden:

content/overview
content/components
content/forces_and_factions
content/creating_a_guild
content/units_and_characteristics
content/setup
content/event_phase
content/activation_phase
content/end_phase
content/activation_and_actions
content/abilities
content/conditions
content/heroes
content/the_arcane
content/formations
```

```{toctree}
  :maxdepth: 4
  :caption: Campaign Play
  :hidden:

content/campaigns
content/quests_and_encounters
content/monsters
content/artifacts
```

```{toctree}
  :maxdepth: 4
  :caption: Appendices
  :hidden:

content/campaigns/intro
content/campaigns/temple_of_Arba
```

