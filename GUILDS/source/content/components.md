
# Components

If you’ve never played a tabletop miniatures wargame before, this section will teach you the basics of the various components you need in order to play *GUILDS*. If you’re a veteran of skirmish wargames, it will still be helpful to clarify the terminology used in *GUILDS* and how it might be similar or different from other games you’ve played before.

## Models

A **Model** is a single physical miniature representing a warrior or creature, such as a dwarf, vampire, smilodon, or even a huge winged demon. Artisan Guild produces a wide variety of outstanding miniatures that you can 3D print yourself or buy from a vendor.

### Bases

Models are almost always mounted on round **Bases**. The size of the base matters for gameplay purposes, so regardless of how the model is posed, make sure the base diameter matches the original size.

In general, infantry units and heroes are mounted on 25mm or 32mm bases. Cavalry and other large creatures are mounted on 50mm bases. Huge creatures are mounted on 100mm bases.

Rarely some models such as chariots aren’t mounted on a base.

## Play Area

Players play a game of *GUILDS* on a tabletop or similar large flat surface called the **Play Area**. The play area is a square measuring 36” (3ft.) on each edge. A 3” margin along the outside edges is reserved as the **Card Area**, where players arrange their cards. The remaining 30” square in the center of the play area is called the **Battlefield**. Models on the battlefield activate, move, and fight over objectives.

### Removed from Play

If something is removed from the battlefield or play area, set it aside in a common area. For most tokens and counters, they go into a common supply. For models, they go back to their owner’s card zone, on top of the model’s respective unit card.

## Dice

All dice rolls in GUILDS use regular six-sided dice, also referred to as d6. Sometimes, when rolling one or more dice, the  notation ‘1d6’ is used to indicate one die, while 2d6 means two dice, 3d6 means three dice, etc.

The outcome of many types of actions, such attacks, casting spells, and tests are determined by a dice roll. When a die is rolled, the value showing on the top face is the **Result**. Compare the result to the **Target Number** of the roll to determine if the die generated a **Success**.

### Rerolls

Some special rules or effects allow one or more dice to be **Rerolled**. When a die is rerolled, pick it up and roll it again, ignoring the first result and taking the second result. A die can never be rerolled more than once. If more than one effect allows dice to be rerolled, reroll all of those dice once at the same time.

## Measurement

To measure the distance between two models or some other range, players use some form of **Measuring Tool**. Plastic or cardboard tools with various lengths in 1” increments can help to verify how far something is. Alternatively, players can use a tape measure or ruler.

Players can measure distances at any time for any reason.

## Counters & Tokens

Players use various small indicators to keep track of values such as wounds and states such as conditions throughout the game.

A **Counter** is a small object, such as a number of glass beads or a spinning dial labeled with numbers, that tracks the numerical value of something as it changes, such as wounds, fatigue, and torment. It’s recommended that players avoid using d6 dice for counters, since they can be easily confused and picked up by accident.

A **Token** is a small indicator with an icon or recognizable color or shape that shows if a model has a particular condition.

## Cards

Players use a deck of cards to represent their guildhall and the units under their command. Cards are laid out in a player’s card zone during the game and used as a reference or to track which units have activated in a round.

All cards are public information. Any player should be able to read a card at any time.

<div class="break-page"></div>
