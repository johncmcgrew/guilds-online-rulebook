Epic Boss Monsters
==================

Epic Bosses are powerful and frightening huge monstrosities. They act like regular Monsters: they are Activated during the End Phase and follow their Behavior.  
However, Bosses Behaviors are much more intricate: positioning is key to defeat them.
An Epic Boss is the last model to be Activated on the table, it takes its turn after all the other Monsters.
Epic Bosses, much like Heroes, get an HW (Heroic Wound) every time they suffer enough Wounds to match their MIGHT, they are not slain so easily.

## Epic Boss Behaviors

Unlike the simple Monsters, an Epic Boss Behavior is composed of a list of up to 6 possible Behaviors that represent its possible actions and skills, following the 6 rules seen before for monsters.  

During the very First Activation of the Epic Boss in a Quest, it attempts to accomplish its First Behavior (n°1 in its list).  
As the Epic Boss ends its first Activation attempting the 1st Behavior, its Next Round Behavior is immediately rolled with a D6.  

This means Epic Bosses actions are always rolled one Round before, so you can adapt to it with anticipation and plan a strategy.

```{hint}
Example: at the end of the 1st Round the Ashen Manticore Epic Boss hints a Cone of Fire for the end of the next Round centered to a specific Mercenary, you move away from him during the next Round all possible ally, isolating the target and covering him behind a Barrier.  
Knowing the direction the Boss will take, you circle around the Manticore in the opposite direction of its target with a dedicated Hero to cut his tail off!
```

Use a Token or dice on the Epic Boss Card to mark the Behavior it is going to accomplish during its next Activation so everyone can adapt.  
Mark also with an Epic Boss Token which spot or model is the target of its next Attack.

## Boss Active Abilities

Epic Bosses Active Abilities are Behaviors on the Boss Card, such as Fire Cone or Troll Jump.  
When these are rolled from the Epic Boss Behaviors, the Boss Moves to approach the target at the maximum range of the rolled Ability/Attack and then unleash it.

If that Ability is going to automatically fail and a nearest target is impossible to reach, the Epic Boss immediately unleashes the rolled Behavior to the nearest target and ends its Activation.  
Not all the Active Boss Abilities are hostile.

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=5 class="title"><h3>Forest Troll Boss</h3><i>Epic Boss, Epic Beast, Towering, Troll</i></td>
      </tr>
      <tr>
        <td colspan=5 class="list">
          <ul class="in-card">
            <li class="in-table"><b>1/2:</b> Smash and Bite<br/>Charge Nearest. If the ATK roll results in a Scale, the target model is swallowed (immediately Vanquished)! Does not work on Heroic Models or Towering Models.</li>
            <li class="in-table"><b>3/4:</b> Forest Troll Jump<br/>Jump (Levitation) over the nearest Ranged Attacker model within 10” and inflict it 4 ATK.</li>
            <li class="in-table"><b>5:</b> Frightening Growl<br/>Each model within a 6&quot; who doesn’t surpass a WILL Test is Terrified.</li>
            <li class="in-table"><b>6:</b> Spinning Attack<br/>Charge Nearest. Roll 3 ATK AoE on every Engaged model.</li>
          </ul>
        </td>
      </tr>
      <tr>
         <td colspan=1><b>HW</b><br/>7</td>
         <td colspan=1><b>Might</b><br/>7(R)</td>
         <td colspan=1><b>DEX</b><br/>4</td>
         <td colspan=1><b>DEF</b><br/>5</td>
         <td colspan=1><b>WILL</b><br/>6</td>
      </tr>
      <tr>
         <td colspan=5>Troll Fangs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <b>3 ATK</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   0"</td>
      </tr>
      <tr>
         <td colspan=5><b>Ignited (6 HW)</b><br/>Troll Regeneration heals from 2 Wounds instead of 1. The model rolls +1 ATK.</td>
      </tr>
   </tbody>
</table>
</div>
<br/>

```{hint}
Example of Epic Boss Card, with his special rolled Behaviors and characteristics.
```

## Ignited Bosses

An Epic Boss Ignites when it accumulates a certain number of HW (Heroic Wounds), written in brackets.  
Ignited Bosses become extremely dangerous, even for your Guildmaster!  
Ignited Bosses don’t go back to their normal state unless written otherwise in their card: not even if they heal from their HW.

<div class="break-page"></div>
