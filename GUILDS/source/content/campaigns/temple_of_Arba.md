The Temple of Arba
==================

<div> <h3>Campaign 1</h3> </div>

_In the lands of Rotvar, an ancient temple has emerged from the depths.
Although many adventurers and armies have tried to enter the ruins, the entrance is sealed by a strange spell._  

_An ancient text found by your Guild's Arcanists describes the Temple of Arba, which seems to coincide with the ruins found, as well as reporting on how to break the seal; but the surprises don't end there!_  

_The text tells of a relic belonging to the time when the Gods walked Mundus: an enchanted artifact located in the center of the temple, protected by an ancient ritual that must be formulated in order to be summoned._  
_Thanks to the information you have obtained, you decide to set off with your guild to conquer the Temple ruins, but your spies have informed you that other forces wish to seize the treasureand have discovered, just like you, the way to break the spell._  

_Realizing that there is not a minute to lose, you gather your Guild and set off for the Temple of Arba._  

_After an endless march through the ancient Rotvar forests you and your Guild reach your destination: you can clearly see the ancient Alfar ruins in the distance, but unfortunately that is not the only thing you can see._  
_A short way from you another group of adventurers has arrived. It seems that the time for battle has come!_


<div class="break-page"></div>

```{toctree}
  :maxdepth: 4
  :hidden:


ToA_Q1
ToA_Q2
ToA_Q3
ToA_Q4
```
