## Whoever enters first wins!

<div><h3>Campaign 1 - Quest 1</h3></div>

_We must reach the entrance to the temple and recite the ancient formula for breaking the seal.
We have to be very careful, the rival Guild seems very fierce and she will do everything to stop us._

### Quest Map

![ToA Q1 Map](../../_static/campaigns/ToA_Q1.png)

### Special Rules

* The Temple Entrance is a 10x10” area, it must be located on the center of a side on the Quest Map.
* Deployment: The Two Guilds must deploy to the opposite side of the Temple Entrance Area as shown on the quest map, within 10" of their own side.

### Main Objective

* The Guild who controls the Temple Entrance Area at the end of 5th rounds wins.

### Quest Rewards

* **Quest Participation**: +1 Guildhall Card.
* **Quest Winner**: +1 Guildhall Card.

<div class="break-page"></div>
