Artifact Cards
==============

Once you get the Arcane Forge Guildhall Card, you can equip 1 Artifact Card to every Heroic Model of your Guild, choosing from the list that you’ll find at the end of this rulebook.  
At the end of a Quest you can discard an Artifact Card from a Hero and get him another one from the list.  
Each Artifact possesses a Tag, and only Heroic figures with the same Tag can equip it.
Artifacts with more Tags can be equipped by every model with that Tag.  
To Equip an Artifact, simply put the Card below the Heroic Card.

## Champions

In a world in which forces are incessantly confronted, where the interests of one prevail over the interests of the other, even the single individual can make a difference.  
Fame and glory await all the fearless who will perform great deeds on the battlefield.  

Once you get the Forge Guildhall Card, if an Infantry or Cavalry model which is not in formation Vanquishes a Hero, Heroic Mount, Heroic Beast, or Epic Boss, and survives to the end of the Quest, can be promoted to Champion.  
To do so, you must equip the Artifact Card Champion to the Card. Remember that only one model of the Infantry or Cavalry is promoted to Champion, and you have to specify which one, making sure it’s easy to identify it from all players.  
Champions get a lot of bonuses, like the ability to perform Heroic Reactions or to get Heroic Wound.

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=4 class="title"><h3>Champion</h3><i>Artifact, Infantry, Cavalry</i></td>
      </tr>
      <tr>
         <td colspan=4>+2 Heroic Wounds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <b>+2 ATK</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <b>+1 WILL</b></td>
      </tr>
      <tr>
         <td colspan=4 class="list">One of the models of this Card is elected Champion: It is considered a Hero, but it can go in Formation.<br/>It cannot become Guildmaster or ride Heroic Mounts.</td>
      </tr>
   </tbody>
</table>
</div>
<br/>

By performing certain actions, such as Heroic Dodge, the Champion may exit its formation.  
When the model elected as Champion is Vanquished, remove also this Artifact from its card.
In a campaign, the Champion model is replaced by a regular one during the next Quests.

<div class="break-page"></div>
