# Overview

This **Core Rulebook** introduces you to the world of Mundus and teaches you everything you need to know to start playing ***GUILDS***.

## Components

A tabletop miniatures wargame uses **Models** representing warriors and creatures on a playing area called the **Battlefield**.
Players move and attack with their models, making use of **Measuring Tools** to check range and **Dice** to determine the outcome of game effects.
**Tokens** and **Cards** are used to keep track of the action.

## Forces & Factions

Mundus is beset by five competing **Forces**, each vying for supremacy.
Within each force, various **Factions** are united by a common devotion to a guiding principle such as dominance or revenge,  or pragmatic concerns like survival and pillage.

## Creating a Guild

When you join a campaign or prepare to face off against an opponent, you must first create a **Guild**, a band of warriors led by a charismatic or ruthless leader called a **Guildmaster**. Then you select a number of **Guildhall Cards** to expand your guild’s infrastructure and **Unit Cards** to bolster your guild’s ranks of infantry and cavalry.

## Units & Characteristics

Each warrior in your guild has **Characteristics**, numerical values representing their capabilities, listed on their unit card. Every warrior also wields one or more **Weapons**, and has some special **Abilities**. A warrior or creature could have some peculiar **Resistance** or **Weakness** to certain kinds of damage.

## Quests & Encounters

You can compete in a stand alone **Skirmish** against an opponent in a single match, or conduct a long-term **Campaign** as a group. Throughout a campaign, you’ll embark on **Quests**, each of which is a linked set of **Encounters** with other players’ guilds. An encounter acts as the rules for setting up a game of ***GUILDS***, and the outcome of the match rewards the players with spoils such as additional guildhall cards to expand your influence.

## Game Structure

In order to play a match of ***GUILDS***, players follow the game’s structure. Start with setting up the battlefield according to the encounter’s **Setup** rules. Each game **Round**, players step through the **Phases**, first resolving the **Event Phase**, then taking turns activating units during the **Activation Phase**, and finally resolving the **End Phase**. The match concludes with one player seizing the Victory Conditions described by the encounter.

## Activation & Actions

Warriors on the battlefield can charge into melee to slay their foes, scramble over ruins for cover to set up an ambush, or conjure horrific magic to lay waste to their hapless victims. During the activation phase, players take turns **Activating** one of their units and performing **Actions** such as moving, attacking, and casting spells.

## Abilities

Each unit has one or more special **Abilities**, making them stand out from rank and file soldiers or enabling a hero to perform extraordinary feats.

## Conditions

**Conditions** encompass anything that might happen to a warrior that creates an ongoing effect, tracked by **Condition Tokens**. Some magic can freeze warriors in place or set them ablaze, a venomous monster could inject deadly toxin into its victims, or a sinister psionic creature might levitate menacingly above the battlefield.

## Heroes

Some unique warriors and creatures have a great destiny before them. A **Hero** stands apart from other units, able withstand more punishment due to their **Heroism Characteristic**, and perform **Heroic Reactions** in response to enemy attacks at the cost of accumulating **Fatigue**. The Guildmaster can give special **Commands** to rally and inspire their warriors. Certain beasts can be ridden as **Heroic Mounts** to carry a hero into battle.

## The Arcane

The ways of magic can be unpredictable and dangerous, but the power to enthrall or incinerate one's enemies tempts many heroes to dabble in the arcane. An **Arcanist** hero has one or more **Arcane Proficiencies**, enabling them to cast spells. Using magic causes an accumulation of **Torment**, which can overwhelm unlucky or incautious arcanists.

## Campaigns

For players looking to engage in long-term narrative play, a **Campaign** enables a group of players to grow and develop their guilds over a series of quests. Heroes and units might acquire Artifacts to enhance their abilities, but could also suffer lingering injuries or curses collectively referred to as **Doom**.

## Formations

When the scale of battle grows beyond small skirmishes, players may wish to use **Formations**, a set of optional rules that introduces organized regiments of large numbers of soldiers for mass battle scenarios.

<div class="break-page"></div>



